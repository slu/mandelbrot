#!/usr/bin/env perl

use strict;
use warnings;

# See https://www.effectiveperlprogramming.com/2015/04/use-v5-20-subroutine-signatures/
use v5.20;
use feature qw(signatures);
no warnings qw(experimental::signatures);

use GD::Simple;
use Getopt::Long;


my $width = 256;
my $max_iterations = 256;
my $color_scheme = "palette";

GetOptions("width=i"          => \$width,
           "max-iterations=i" => \$max_iterations,
           "color-scheme=s"   => \$color_scheme)
  or die("Usage: $0 [--width <width>] [--max-iterations <max iterations>] [--color-scheme <color scheme>]\n");

my $aspect_ratio = 1.6;
my $color_sub=\&{"color_$color_scheme"};
my $height = int($width / $aspect_ratio);
my ($x_scale, $y_scale) = (3.5/$width, 2/$height);
my ($x_offset, $y_offset) = (-2.5, -1);


sub color_gray($iteration) {
    my $rgb = $iteration < $max_iterations ? int(255 * ($max_iterations - $iteration) / $max_iterations) : 0;
    return ($rgb, $rgb, $rgb);
}


sub color_gradient($iteration) {
    my $red = $iteration < $max_iterations ? 127 : 0;
    my $blue = $iteration < $max_iterations ? int(255 * ($max_iterations - $iteration) / $max_iterations) : 0;
    my $green = $iteration < $max_iterations ? int(255 * $iteration / $max_iterations) : 0;
    return ($red, $green, $blue);
}


sub color_gradient2($iteration) {
    # Formular generated using https://mycurvefit.com/ with the
    # following values:
    #
    #       X   Y
    #     -------
    #       0   0
    #     128  16
    #     192  64
    #     256 256
    #
    my $val = 101836900+(3.045016-101836900)/(1+($iteration/3637.774)**4.863124);
    #my $val = 101836900-101836896.955/(1+($iteration/3637.774)**4.863124);
    return $iteration < $max_iterations ?
      ($val, 127, 256-$val) :
      (0,0,0);
}


sub color_hsv($iteration) {
    # From https://www.codingame.com/playgrounds/2358/how-to-plot-the-mandelbrot-set/adding-some-colors
    my $hue = int(255 * $iteration / $max_iterations);
    my $saturation = 255;
    my $value = $iteration < $max_iterations ? 255 : 0;
    return GD::Simple->HSVtoRGB($hue, $saturation, $value);
}


sub color_c64($iteration) {
    # From https://www.c64-wiki.com/wiki/Color
    state @palette = (
        [  0,   0,   0],  # Black
        [255, 255, 255],  # White
        [136,   0,   0],  # Red
        [170, 255, 238],  # Cyan
        [204,  68, 204],  # Purple
        [  0, 204,  85],  # Green
        [  0,   0, 170],  # Blue
        [238, 238, 119],  # Yellow
        [221, 136,  85],  # Orange
        [102,  68,   0],  # Brown
        [255, 119, 119],  # Light red
        [ 51,  51,  51],  # Dark grey
        [119, 119, 119],  # Grey 2
        [170, 255, 102],  # Light green
        [  0, 136, 255],  # Light blue
        [187, 187, 187]); # Light grey
    state $palette_size = scalar @palette;

    my $i = $iteration % $palette_size;

    return ($palette[$i][0], $palette[$i][1], $palette[$i][2]);
}


sub color_palette($iteration) {
    # From https://stackoverflow.com/a/16505538
    state @palette = (
        [ 66,  30,  15],  # brown 3
        [ 25,   7,  26],  # dark violett
        [  9,   1,  47],  # darkest blue
        [  4,   4,  73],  # blue 5
        [  0,   7, 100],  # blue 4
        [ 12,  44, 138],  # blue 3
        [ 24,  82, 177],  # blue 2
        [ 57, 125, 209],  # blue 1
        [134, 181, 229],  # blue 0
        [211, 236, 248],  # lightest blue
        [241, 233, 191],  # lightest yellow
        [248, 201,  95],  # light yellow
        [255, 170,   0],  # dirty yellow
        [204, 128,   0],  # brown 0
        [153,  87,   0],  # brown 1
        [106,  52,   3]); # brown 2
    state $palette_size = scalar @palette;

    my $i = $iteration % $palette_size;

    return $iteration < $max_iterations ?
      ($palette[$i][0], $palette[$i][1], $palette[$i][2]) :
      (0,0,0);
}


my $img = GD::Simple->new($width,$height);

for my $py (0..$height-1) {
    for my $px (0..$width-1) {
        my ($xz, $yz) = ($px * $x_scale + $x_offset, $py * $y_scale + $y_offset);
        my ($x, $y) = (0, 0);
        my $iteration;
        for (1..$max_iterations) {
            $iteration = $_;
            last if $x*$x+$y*$y > 4;
            ($x, $y) = ($x**2 - $y**2 + $xz, 2 * $x * $y + $yz);
        }
        $img->fgcolor($color_sub->($iteration));
        $img->rectangle($px, $py, $px, $py);
    }
}

my $filename = "mandelbrot-${color_scheme}-${width}-${max_iterations}.png";
open my $out, '>', $filename or die;
binmode $out;
print $out $img->png;
say "Saved to '$filename'";
