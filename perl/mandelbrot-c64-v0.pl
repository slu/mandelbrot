#!/usr/bin/env perl


use if $^O eq "MSWin32", "Win32::Console::ANSI";
use Term::ANSIColor qw(:constants);
use Term::ANSIScreen qw/:cursor :screen/;

@colors = (BLACK,        RED,            GREEN,        YELLOW,
           BLUE,         MAGENTA,        CYAN,         WHITE,
           BRIGHT_BLACK, BRIGHT_RED,     BRIGHT_GREEN, BRIGHT_YELLOW,
           BRIGHT_BLUE,  BRIGHT_MAGENTA, BRIGHT_CYAN,  BRIGHT_WHITE);

cls();                                           # 10 print chr$(147)
for $py (0..21) {                                # 100 for py=0 to 21
for $px (0..31) {                                # 110 for px=0 to 31
$xz = $px*3.5/32-2.5;                            # 120 xz = px*3.5/32-2.5
$yz = $py*2/22-1;                                # 130 yz = py*2/22-1
$x = 0;                                          # 140 x = 0
$y = 0;                                          # 150 y = 0
$i = 0; for (0..14) {                            # 160 for i=0 to 14
last if $x*$x+$y*$y > 4;                         # 170 if x*x+y*y > 4 then goto 215
$xt = $x*$x - $y*$y + $xz;                       # 180 xt = x*x - y*y + xz
$y = 2*$x*$y + $yz;                              # 190 y = 2*x*y + yz
$x = $xt;                                        # 200 x = xt
$i = $_;}                                        # 210 next i
$i--;                                            # 215 i = i-1
locate $py+1,2*$px+1;                            # 220 poke 1024+py*40+px,160
print $colors[$i].REVERSE.'  '.RESET;            # 230 poke 55296+py*40+px,i
}                                                # 240 next px
}                                                # 260 next py
for (0..9) {                                     # 270 for i=0 to 9
up;                                              # 280 print chr$(17)
}                                                # 290 next i
