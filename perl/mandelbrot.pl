#!/usr/bin/env perl

# Displays the Mandelbrot Set using ANSI escape codes.

# The Mandelbrot Set will be scaled to fit the terminal size.  When
# done, the script will wait for user input. In other words, you
# should press <return> to exit.

use strict;
use warnings;

use if $^O eq "MSWin32", "Win32::Console::ANSI";

use Term::ANSIColor qw(:constants);
use Term::ANSIScreen qw/:cursor :screen/;
use Term::Size::Any;

# From https://en.wikipedia.org/wiki/ANSI_escape_code

my $ANSI_HIDE_CURSOR="\e[?25l";
my $ANSI_SHOW_CURSOR="\e[?25h";

my @COLORS = (BRIGHT_WHITE, WHITE, BRIGHT_CYAN, BRIGHT_BLUE, CYAN,
              BRIGHT_YELLOW, BLUE, YELLOW, GREEN, BRIGHT_GREEN,
              BRIGHT_MAGENTA, MAGENTA, BRIGHT_RED, RED, BLACK);

my ($columns, $rows) = Term::Size::Any::chars;

# https://stackoverflow.com/questions/6565703/math-algorithm-fit-image-to-screen-retain-aspect-ratio
my $term_aspect_ration = $columns / $rows;
my $aspect_ratio = 1.6;
my ($width, $height) = $term_aspect_ration > $aspect_ratio ? (int($aspect_ratio * $rows), $rows) : ($columns, int($aspect_ratio * $columns));

my ($x_scale, $y_scale) = (3.5/$width, 2/$height);
my ($x_offset, $y_offset) = (-2.5, -1);
my $max_iterations = 1024;

cls();

locate 1,1;

for my $py (0..$height-1) {
    for my $px (0..$width-1) {
        my ($xz, $yz) = ($px * $x_scale + $x_offset, $py * $y_scale + $y_offset);
        my ($x, $y) = (0, 0);
        my $iteration;
        for (1..$max_iterations) {
            $iteration = $_;
            last if $x**2+$y**2 > 4;
            ($x, $y) = ($x**2 - $y**2 + $xz, 2 * $x * $y + $yz);
        }
        my $color = $iteration < $max_iterations ?
          $COLORS[$iteration % 15] :
          BRIGHT_BLACK;
        print $color.REVERSE.'  '.RESET;
    }
    print "\n" if $py < $height-1;
}

print $ANSI_HIDE_CURSOR;
locate 1,1;
<STDIN>; # wait for return
print $ANSI_SHOW_CURSOR;
