CSI = "\033[" # Control Sequence Introducer

CLS = f"{CSI}2J"
HOME = f"{CSI}H"

BLACK = f"{CSI}30m"
RED = f"{CSI}31m"
GREEN = f"{CSI}32m"
YELLOW = f"{CSI}33m"
BLUE = f"{CSI}34m"
MAGENTA = f"{CSI}35m"
CYAN = f"{CSI}36m"
WHITE = f"{CSI}37m"

BRIGHT_BLACK = f"{CSI}90m"
BRIGHT_RED = f"{CSI}91m"
BRIGHT_GREEN = f"{CSI}92m"
BRIGHT_YELLOW = f"{CSI}93m"
BRIGHT_BLUE = f"{CSI}94m"
BRIGHT_MAGENTA = f"{CSI}95m"
BRIGHT_CYAN = f"{CSI}96m"
BRIGHT_WHITE = f"{CSI}97m"

REVERSE = f"{CSI}7m"
RESET = f"{CSI}0m"

HIDE_CURSOR = f"{CSI}?25l"
SHOW_CURSOR = f"{CSI}?25h"


def cls():
    print(CLS, end="")


def locate(row, column):
    print(f"{CSI}{row};{column}H", end="")
