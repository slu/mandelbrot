#!/usr/bin/env python

import argparse

from PIL import Image

#width = 
#max_iterations = 1024

parser = argparse.ArgumentParser(description='Render Mandelbrot Set')
parser.add_argument('--width', type=int, default=256)
parser.add_argument('--max-iterations', type=int, default=1024)
args = parser.parse_args()

aspect_ratio = 1.6
height = int(args.width / aspect_ratio)
(x_scale, y_scale) = (3.5/args.width, 2/height)
(x_offset, y_offset) = (-2.5, -1)


def color_gray(iteration):
    rgb = int(255 * (args.max_iterations - iteration) / args.max_iterations) if iteration < args.max_iterations else 0
    return (rgb, rgb, rgb)

PALETTE = None
def color_palette(iteration):    
    # From https://stackoverflow.com/a/16505538
    # pylint: disable=global-statement
    global PALETTE
    if PALETTE is None:
        PALETTE = [
            ( 66,  30,  15),  # brown 3
            ( 25,   7,  26),  # dark violett
            (  9,   1,  47),  # darkest blue
            (  4,   4,  73),  # blue 5
            (  0,   7, 100),  # blue 4
            ( 12,  44, 138),  # blue 3
            ( 24,  82, 177),  # blue 2
            ( 57, 125, 209),  # blue 1
            (134, 181, 229),  # blue 0
            (211, 236, 248),  # lightest blue
            (241, 233, 191),  # lightest yellow
            (248, 201,  95),  # light yellow
            (255, 170,   0),  # dirty yellow
            (204, 128,   0),  # brown 0
            (153,  87,   0),  # brown 1
            (106,  52,   3)]  # brown 2

    i = iteration % len(PALETTE)

    return PALETTE[i] if iteration < args.max_iterations else (0,0,0)

color_func=color_palette

img = Image.new('RGB', (args.width, height))

for py in range(height-1):
    for px in range(args.width-1):
        (xz, yz) = (px * x_scale + x_offset, py * y_scale + y_offset)
        (x, y) = (0, 0)
        for iteration in range(1, args.max_iterations):
            if x*x+y*y > 4:
                break
            (x, y) = (x**2 - y**2 + xz, 2 * x * y + yz)

        img.putpixel((px,py), color_func(iteration))

filename = f"mandelbrot-{args.width}-{args.max_iterations}.png"
img.save(filename)
