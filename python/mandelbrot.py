#!/usr/bin/env python

"""Displays the Mandelbrot Set using ANSI escape codes.

The Mandelbrot Set will be scaled to fit the terminal size.  When
done, the script will wait for user input. In other words, you should
press <return> to exit.

The code is an almost line-by-line conversion of the following Perl
script:

https://github.com/soren/perl-examples/blob/main/mandelbrot/mandelbrot.pl

"""

import os

# pylint: disable=wildcard-import,unused-wildcard-import
from term.ansiscreen import *

os.system("")  # enables ansi escape characters in terminal

COLORS = [BRIGHT_WHITE,   WHITE,          BRIGHT_CYAN,
          BRIGHT_BLUE,    CYAN,           BRIGHT_YELLOW,
          BLUE,           YELLOW,         GREEN,
          BRIGHT_GREEN,   BRIGHT_MAGENTA, MAGENTA,
          BRIGHT_RED,     RED,            BLACK]

size = os.get_terminal_size()
(width, height) = (int(1.6 * size.lines), int(size.lines))
(x_scale, y_scale) = (3.5/width, 2/height)
(x_offset, y_offset) = (-2.5, -1)
max_iterations = 1024

cls()
locate(1,1)

for py in range(height-1):
    for px in range(width-1):
        (xz, yz) = (px * x_scale + x_offset, py * y_scale + y_offset)
        (x, y) = (0, 0)
        for iteration in range(1, max_iterations+1):
            if x*x+y*y > 4:
                break
            (x, y) = (x**2 - y**2 + xz, 2 * x * y + yz)
        color = COLORS[iteration % 15] if iteration < max_iterations else BRIGHT_BLACK
        print(color+REVERSE+'  '+RESET, end='')
    if py < height - 1:
        print()

print(HIDE_CURSOR, end="")
locate(1,1)
input()
print(SHOW_CURSOR, end="")
