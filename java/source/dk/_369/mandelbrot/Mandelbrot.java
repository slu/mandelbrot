package dk._369.mandelbrot;

import java.util.Scanner;

import static dk._369.term.Ansi.*;
import static dk._369.term.Size.*;

public class Mandelbrot {

    private static String COLORS[] = {BRIGHT_WHITE,   WHITE,          BRIGHT_CYAN,
                                      BRIGHT_BLUE,    CYAN,           BRIGHT_YELLOW,
                                      BLUE,           YELLOW,         GREEN,
                                      BRIGHT_GREEN,   BRIGHT_MAGENTA, MAGENTA,
                                      BRIGHT_RED,     RED,            BLACK};

    public static void main(String[] args) {
        new Mandelbrot().run();
    }

    public void run() {
        double termAspectRation = cols() / rows();
        double aspectRatio = 1.6;
        int width, height;

        if (termAspectRation > aspectRatio) {
            width = (int)(aspectRatio * rows());
            height = rows();
        } else {
            width = cols();
            height = (int)(aspectRatio * cols());
        }

        double xScale = 3.5/width;
        double yScale = 2.0/height;
        double xOffset = -2.5;
        double yOffset = -1.0;
        int maxIterations = 1024;
        
        cls();

        locate(1,1);

        for (int py=0; py<height; py++) {
            for (int px=0; px<width; px++) {
                double xz = px * xScale + xOffset, yz = py * yScale + yOffset;
                double x = 0, y = 0;
                int iteration;
                for (iteration = 1; iteration <= maxIterations; iteration++) {
                    if (x*x + y*y > 4) {
                        break;
                    }
                    double xTmp = x*x - y*y + xz;
                    y = 2.0 * x * y + yz;
                    x = xTmp;
                }
                String color = iteration < maxIterations ?
                    COLORS[iteration % 15] :
                    BRIGHT_BLACK;
                
                System.out.print(color+REVERSE+"  "+RESET);
            }
            if (py < height-1) {
                System.out.println();
            }
        }

        System.out.print(HIDE_CURSOR);
        locate(1,1);
        new Scanner(System.in).nextLine();
        System.out.print(SHOW_CURSOR);
    }
}
