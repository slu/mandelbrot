package dk._369.term;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public final class Size {    
    private static final int DEFAULT_ROWS = 24;
    private static final int DEFAULT_COLS = 80;

    private static final String ROWS_PROPERTY = "Rows";
    private static final String COLS_PROPERTY = "Columns";
            
    private static final int rows;
    private static final int cols;

    static {
        try {
            int[] dimensions = retrieveConsoleDimensions();

            if (dimensions.length == 2) {
                rows = dimensions[0];
                cols = dimensions[1];
            } else {
                rows = DEFAULT_ROWS;
                cols = DEFAULT_COLS;
            }
        } catch(Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static int[] retrieveConsoleDimensions() throws IOException {
        if (System.getProperty("os.name").toLowerCase().contains("linux")) {
            return retrieveConsoleDimensionsOnLinux();
        }
        return retrieveConsoleDimensionsFromProperties();
    }

    private static int[] retrieveConsoleDimensionsOnLinux() throws IOException {
        int rows, cols;

        Pattern dimensionsPattern = Pattern.compile("(?<rows>\\d+) (?<cols>\\d+)");
        String output = runSsty();

        Matcher matcher = dimensionsPattern.matcher(output);
        if (matcher.matches()) {
            rows = Integer.parseInt(matcher.group("rows"));
            cols = Integer.parseInt(matcher.group("cols"));
            return new int[] {rows, cols};
        }

        return new int[0];
    }

    private static String runSsty() throws IOException {
        ProcessBuilder pb = new ProcessBuilder("/usr/bin/env", "stty", "size");
        pb.redirectInput(ProcessBuilder.Redirect.from(new File("/dev/tty")));
        Process process = pb.start();
        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

        return reader.readLine();
    }

    private static int[] retrieveConsoleDimensionsFromProperties() {
        int rows, cols;
        Properties properties = System.getProperties();

        if (properties.containsKey(ROWS_PROPERTY) && properties.containsKey(COLS_PROPERTY)) {
            rows = Integer.parseInt(System.getProperty(ROWS_PROPERTY));
            cols = Integer.parseInt(System.getProperty(COLS_PROPERTY));
            return new int[] {rows, cols};
        } else {
            return new int[0];
        }
    }

    public static int rows() {
        return rows;
    }

    public static int cols() {
        return cols;
    }
}
