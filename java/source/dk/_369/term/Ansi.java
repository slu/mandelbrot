package dk._369.term;

public final class Ansi {
    public static final String CSI = "\033["; // Control Sequence Introducer

    public static final String CLS = CSI + "2J";
    public static final String HOME = CSI + "H";

    public static final String BLACK = CSI + "30m";
    public static final String RED = CSI + "31m";
    public static final String GREEN = CSI + "32m";
    public static final String YELLOW = CSI + "33m";
    public static final String BLUE = CSI + "34m";
    public static final String MAGENTA = CSI + "35m";
    public static final String CYAN = CSI + "36m";
    public static final String WHITE = CSI + "37m";

    public static final String BRIGHT_BLACK = CSI + "90m";
    public static final String BRIGHT_RED = CSI + "91m";
    public static final String BRIGHT_GREEN = CSI + "92m";
    public static final String BRIGHT_YELLOW = CSI + "93m";
    public static final String BRIGHT_BLUE = CSI + "94m";
    public static final String BRIGHT_MAGENTA = CSI + "95m";
    public static final String BRIGHT_CYAN = CSI + "96m";
    public static final String BRIGHT_WHITE = CSI + "97m";

    public static final String REVERSE = CSI + "7m";
    public static final String RESET = CSI + "0m";
    
    public static final String HIDE_CURSOR = CSI + "?25l";
    public static final String SHOW_CURSOR = CSI + "?25h";

    public static void cls() {
        System.out.print(CLS);
    }

    public static void locate(int row, int column) {
        System.out.print(String.format("%s%d;%dH", CSI, row, column));
    }
}
