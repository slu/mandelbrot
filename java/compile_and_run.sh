#!/usr/bin/env bash

set -eux

source_dir=source
build_dir=build
main_class=dk._369.mandelbrot.Mandelbrot

javac -d "$build_dir" --class-path "$source_dir"  "${source_dir}/${main_class//\./\/}.java"

java --class-path "$build_dir" "$main_class"
