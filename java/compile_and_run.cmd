set SOURCE_DIR=source
set BUILD_DIR=build
set MAIN_CLASS=dk._369.mandelbrot.Mandelbrot

javac -d %BUILD_DIR% --class-path %SOURCE_DIR% "%SOURCE_DIR%/%MAIN_CLASS:.=\%.java" || goto:eof

for /f "tokens=1,2,3 delims=:, " %%w in ('powershell -command "&{(get-host).ui.rawui}"') do @if %%w == WindowSize (set COLS=%%x & set ROWS=%%y)

java --class-path %BUILD_DIR% -DColumns=%COLS% -DRows=%ROWS% %MAIN_CLASS%
