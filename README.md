[![License: GPL v3](https://img.shields.io/badge/License-GPLv3-blue.svg)](https://www.gnu.org/licenses/gpl-3.0) [![Written in Perl](https://img.shields.io/badge/Language-Perl-blue)](https://www.perl.org/) [![Written in Python](https://img.shields.io/badge/Language-Python-blue)](https://www.python.org/) [![Written in Java](https://img.shields.io/badge/Language-Java-blue)](https://www.java.com/)
----
Mandelbrot
==========

I've been watching Matt Heffernan's *8-bit Battle Royale!*[^1] videos
on YouTube. Matt is comparing Mandelbrot set-generating programs
written for different 8-bit computers. I highly recommend the
videos. They inspired me to write Perl scripts that could generate
Mandelbrot sets[^2], which is what you'll find in the
[perl](perl) directory. After playing around in Perl, I wrote
versions in Python and Java. You'll find the source code in the [python](python)
and [java](java) directories.


Line-by-line Conversions of the C64 BASIC Program
-------------------------------------------------

I started by converting the C64 BASIC program[^3] that Matt Heffernan
wrote into Perl. I wrote two versions of this:

* `mandelbrot-c64-v0.pl` — written in the style of BASIC with neither
  declarations nor code indention.
* `mandelbrot-c64-v1.pl` — essentially the same code as in v0, but
  I've added strict/warnings pragmas, which forced me to declare all
  variables, and I've indented the code.

Both versions will plot the Mandelbrot Set in the terminal using a
fixed size of 31 columns and 21 rows, and a 16-color palette. The
scripts use the following CPAN modules:

* [Win32::Console::ANSI](https://metacpan.org/pod/Win32::Console::ANSI)
  — to enable ANSI support on Windows
* [Term::ANSIColor](https://metacpan.org/pod/Term::ANSIColor) — to
  define the 16 colors used in the palette
* [Term::ANSIScreen](https://metacpan.org/pod/Term::ANSIScreen) — to
  clear the console and move the cursor

Note: Although Term::ANSIScreen is supposed to be a superset of
Term::ANSIColor, it only seems to define 8 colors, and I wanted 16
colors.


Mandelbrot in the Console
-------------------------

Because the above scripts were written to closely mimic the original
programs by Matt Heffernan, they used a fixed size and used a color
palette that is not optimal (in my terminal at least). This made me
create a new version of the scripts:

* `mandelbrot.pl`

This script will figure out the size of the terminal and fit the
Mandelbrot Set to that size and it includes a redefined palette that
looks good in my terminal. I've also added some variables to make the
code easier to read and I replaced the use of the locate function in
the main loop with calls to print. Finally, the script will wait for
the user to press <enter> to exit.

The script uses one additional CPAN module along with the three
modules mentioned above:

* [Term::Size::Any](https://metacpan.org/pod/Term::Size::Any) —
  retrieve the dimension of the terminal


Creating PNG Images Using GD::Simple
------------------------------------

Next, I wanted to create Mandelbrot Sets as images, so I create the
following script:

* `mandelbrot-gd.pl`

This script will create a PNG image instead of printing it to the
terminal. I command-line options to set the width of the image, the
maximum number of iterations, and the color scheme.

This script uses one CPAN module:

* [GD::Simple](https://metacpan.org/pod/GD::Simple) — to create the
  PNG image

On my Ubuntu machine I also had to install libgd-dev like this:

``` shell
sudo apt-get install -y libgd-dev
```


References
----------

* Badges hosted by https://shields.io/
* GPLv3 badge found at https://gist.github.com/lukas-h/2a5d00690736b4c3a7ba


[^1]: https://youtu.be/DC5wi6iv9io
[^2]: https://en.wikipedia.org/wiki/Mandelbrot_set
[^3]: https://github.com/SlithyMatt/multi-mandlebrot/blob/main/c64/c64-mandlebrot.bas
